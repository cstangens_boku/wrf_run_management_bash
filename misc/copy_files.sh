#!/usr/bin/bash

read -p 'Target directory: ' WRF_DATA_DIR

 #store data
 if [ ! -d /$WRF_DATA_DIR/ ]; then
      mkdir -p /$WRF_DATA_DIR/
  fi
  mv wrfout* $WRF_DATA_DIR
  mv wrfxtrm* $WRF_DATA_DIR
  mv wrfinput* $WRF_DATA_DIR
  mv wrfbdy* $WRF_DATA_DIR
  mv wrflowinp* $WRF_DATA_DIR
  mv wrfbiochemi* $WRF_DATA_DIR
  mv rsl.out.0000 $WRF_DATA_DIR
  mv rsl.error.0000 $WRF_DATA_DIR
  cp namelist.input $WRF_DATA_DIR
