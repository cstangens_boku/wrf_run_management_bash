#!/bin/bash

module purge

source /gpfs/data/fs71449/cschmidt1/wrf/attain/wrf_environment.sh


ys=(00 2045     )
ms=(00 01       )
ds=(00 01       )
ye=(00 2045     )
me=(00 01       )
de=(00 3       )
ts=(00 2      )
re=(00 .false.)

#cd /gpfs/data/fs71449/cschmidt1/wrf/attain/pkg-src/wrf/test/em_real

i=1
imax=22 # number of simulations
while [ $i -le $imax ]; do
    ii=$((i)) # part number
    echo "#### PART"$(printf %02d $ii)
    export SIM_NUM=S4part$(printf %02d $ii)
    export SIMULATION_TIMESPAN=${ts[i]} # simulation timespan in days
    export START_Y=${ys[i]}
    export START_M=${ms[i]}
    export START_D=${ds[i]}
    export START_H=00
    export END_Y=${ye[i]}
    export END_M=${me[i]}
    export END_D=${de[i]}
    export END_H=00
    export TIM_REST=${re[i]}
    export TIM_RESTIN=72000
    export DOM_TS=50
    #echo $(printf %02d $ii),${ts[i]},${ys[i]},${ms[i]},${ds[i]},${ye[i]},${me[i]},${de[i]},${re[i]}
    bash run_management/wrf_run.sh
i=$((i+1))
done

echo "End of managing script"
