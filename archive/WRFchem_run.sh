#!/bin/bash

echo "### "`date`" Starting job $0: WRF clim run"

PROJECTS_DIR=/gpfs/data/fs71449/karlicky3/projects
LOCAL_DATA_DIR=/gpfs/data/fs71449/karlicky3/data
PROJECT=wrfchem
EXP=HCcesm_9km_2007-16_ATTR

WRF_WORK_DIR=$PROJECTS_DIR/$PROJECT/$EXP
WRF_DATA_DIR=$LOCAL_DATA_DIR/$PROJECT/$EXP

export PROJECT
export EXP
export WRF_WORK_DIR
export WRF_DATA_DIR

cd $WRF_WORK_DIR
if [[ `printenv SIM_NUM` ]]; then
    WRF_DATA_DIR=$LOCAL_DATA_DIR/$PROJECT/$EXP/$SIM_NUM
    export WRF_DATA_DIR
    echo "   ..."$SIM_NUM
else
    echo "   ...not managed run"
. ./WRFchem_config.sh
fi
#. ./WRFchem_preproc.sh
#. ./WRFchem_main.sh
jobname=WRFCp$(printf %02d $ii)
sbatch -J $jobname WRFchem_preproc_main.sh
while [[ `squeue -n $jobname | grep $jobname` != "" ]] ; do
    sleep 100;
done

echo "### "`date`" End of script"