#!/bin/bash

#SBATCH -N 4
#SBATCH --partition=mem_0096
#SBATCH --qos=p71391_0096
#SBATCH -A p71391
#SBATCH --ntasks-per-node=45
#SBATCH --ntasks-per-core=1

echo "### "`date`" Starting $0: WRF Preprocesors:"

#cd $WRF_WORK_DIR/run/
cd /gpfs/data/fs71449/karlicky3/projects/wrfchem/HCcesm_9km_2007-16_ATTR/run

echo "### "`date`" Starting $0: 1. real.exe"
export CHEM_INP=0
export CHEM_BIOEM=0
. ../WRFchem_namelist.sh

. /gpfs/data/fs71449/karlicky3/bin/ifortsource.sh
srun -l -N1 -n12 real.exe
#mpirun -n 12 ./real.exe

mv rsl.error.0000 rsl.error.real1
mv rsl.out.0000 rsl.out.real1
if [[ `cat rsl.out.real1 | grep SUCCESS` == "" ]]; then echo "Error occured !!!"; exit; fi

echo "### "`date`" Starting $0: 2. MEGAN"
rm ./megan/DSW.nc
rm ./megan/TAS.nc
ln -s /gpfs/data/fs71449/karlicky3/data/wrfchem_ICBC/MEGAN/cesm-ccsm/DSW_2007-2016_rcp8.5_$START_Y.nc ./megan/DSW.nc
ln -s /gpfs/data/fs71449/karlicky3/data/wrfchem_ICBC/MEGAN/cesm-ccsm/TAS_2007-2016_rcp8.5_$START_Y.nc ./megan/TAS.nc
./megan_bio_emiss < namelist.input > megan_bio_emiss.log

echo "### "`date`" Starting $0: 3. secondly real.exe"
export CHEM_BIOEM=3
. ../WRFchem_namelist.sh
srun -l -N1 -n12 real.exe
#mpirun -n 12 ./real.exe

mv rsl.error.0000 rsl.error.real2
mv rsl.out.0000 rsl.out.real2
if [[ `cat rsl.out.real2 | grep SUCCESS` == "" ]]; then echo "Error occured !!!"; exit; fi

echo "### "`date`" Starting $0: 4. mozbc"
./mozbc < RADM2SORGsocol4.inp > mozbc.out
if [[ `cat mozbc.out | grep completed` == "" ]]; then echo "Error occured !!!"; exit; fi

#echo "### "`date`" 5. link UBC"
#ln -s /net/meop-nas2.priv/volume2/data2/ucci/WRFchem/ICBC_chem/UBC/UBC_inputs/ubvals_b40.20th.track1_1996-2005.nc ./
#ln -s /net/meop-nas2.priv/volume2/data2/ucci/WRFchem/ICBC_chem/UBC/UBC_inputs/clim_p_trop.nc ./

echo "### "`date`" Starting $0: WRF-Chem main: wrf.exe"
export CHEM_INP=1
. ../WRFchem_namelist.sh
ulimit -s unlimited
srun -l -N4 -n180 wrf.exe
#mpirun -n 180 ./wrf.exe
if [[ `cat rsl.out.0000 | grep SUCCESS` == "" ]]; then echo "Error occured !!!"; exit; fi

#store data
if [ ! -d /$WRF_DATA_DIR/ ]; then
 mkdir -p /$WRF_DATA_DIR/
fi
mv wrfout* /$WRF_DATA_DIR/
mv wrfxtrm* /$WRF_DATA_DIR/
mv wrfinput* /$WRF_DATA_DIR/
mv wrfbdy* /$WRF_DATA_DIR/
mv wrflowinp* /$WRF_DATA_DIR/
mv wrfbiochemi* /$WRF_DATA_DIR/
mv rsl.out.0000 /$WRF_DATA_DIR/
mv rsl.error.0000 /$WRF_DATA_DIR/
cp namelist.input /$WRF_DATA_DIR/

echo "### "`date`" Starting $0: End of WRF main run"

cd $WRF_WORK_DIR