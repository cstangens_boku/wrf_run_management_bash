#!/bin/bash

module purge

source /gpfs/data/fs71449/cschmidt1/wrf/attain_2026_45/wrf_environment.sh


ys=(00 2028    2028   2029   2029   )
ms=(00 01      06     01     06     )
ds=(00 01      29     01     29     )
ye=(00 2028    2029   2029   2030   )
me=(00 06      01     06     01     )
de=(00 30      02     30     02     )
ts=(00 180      187     180     187     )
re=(00 .false. .false. .false. .false.)

cd /gpfs/data/fs71449/cschmidt1/wrf/attain_2026_45/pkg-src/wrf/test/em_real 

i=1
imax=4 # number of simulations
while [ $i -le $imax ]; do
    ii=$((i)) # part number
    echo "#### PART"$(printf %02d $ii)
    export SIM_NUM=S4part$(printf %02d $ii)
    export SIMULATION_TIMESPAN=${ts[i]} # simulation timespan in days
    export START_Y=${ys[i]}
    export START_M=${ms[i]}
    export START_D=${ds[i]}
    export START_H=00
    export END_Y=${ye[i]}
    export END_M=${me[i]}
    export END_D=${de[i]}
    export END_H=00
    export TIM_REST=${re[i]}
    export TIM_RESTIN=72000
    export DOM_TS=50
    #echo $(printf %02d $ii),${ts[i]},${ys[i]},${ms[i]},${ds[i]},${ys[i+1]},${ms[i+1]},${ds[i+1]},${re[i]}
    bash run_management/wrf_run.sh
i=$((i+1))
done

echo "End of managing script"
