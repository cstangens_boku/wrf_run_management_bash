#!/bin/bash

#SBATCH -N 4
#SBATCH --partition=mem_0096
#SBATCH --qos=p71391_0096
#SBATCH -A p71391
#SBATCH --ntasks-per-node=45
#SBATCH --ntasks-per-core=1

export wrf_output_dir=/binfl/lv71391/cschmidt/managed_runs/rcp85_46/

export WRF_DATA_DIR=$wrf_output_dir$SIM_NUM

echo "### "`date`" Starting $0: WRF Preprocesors:"


echo "### "`date`" Starting $0: 1. real.exe"
export CHEM_INP=0
export CHEM_BIOEM=0
bash run_management/wrf_namelist.sh

srun -l -N1 -n12 real.exe
mv rsl.error.0000 rsl.error.real1
mv rsl.out.0000 rsl.out.real1


rm megan/megan_data/DSW.nc
rm megan/megan_data/TAS.nc
ln -sf /binfl/lv71391/cschmidt/attain/megan/DSW_2046-2055_rcp8.5_$START_Y.nc megan/megan_data/DSW.nc
ln -sf /binfl/lv71391/cschmidt/attain/megan/TAS_2046-2055_rcp8.5_$START_Y.nc megan/megan_data/TAS.nc
./megan/megan_bio_emiss < namelist.input > megan_bio_emiss.log

echo "### "`date`" Starting $0: 3. second real.exe"

export CHEM_BIOEM=3
bash run_management/wrf_namelist.sh


srun -l -N1 -n12 real.exe


mv rsl.error.0000 rsl.error.real2
mv rsl.out.0000 rsl.out.real2

echo "### "`date`" Starting $0: 4. mozbc"
./mozbc/mozbc < mozbc/RADM2SORGsocol4.inp > mozbc.out

echo "### "`date`" Starting $0: WRF-Chem main: wrf.exe"
export CHEM_INP=1
bash run_management/wrf_namelist.sh
ulimit -s unlimited
srun -l -N4 -n180 wrf.exe

#store data
if [ ! -d $WRF_DATA_DIR ]; then
 mkdir -p $WRF_DATA_DIR
fi
mv wrfout* $WRF_DATA_DIR
mv wrfxtrm* $WRF_DATA_DIR
mv wrfinput* $WRF_DATA_DIR
mv wrfbdy* $WRF_DATA_DIR
mv wrflowinp* $WRF_DATA_DIR
mv wrfbiochemi* $WRF_DATA_DIR
mv rsl.out.0000 $WRF_DATA_DIR
mv rsl.error.0000 $WRF_DATA_DIR
cp namelist.input $WRF_DATA_DIR

<<<<<<< HEAD
echo "### "`date`" Starting $0: End of WRF main run"
=======
echo "### "`date`" Starting $0: End of WRF main run"
>>>>>>> 02a82c5d28bd225fa968a332c3935bfc2825ff7c
